import pandas as pd
import math
# lineList = "COVID 19 LINELIST AS AT 14TH JULY 21.csv"


class CopyCliffData:

    def __init__(self,linelist,caseid):
        self.lineList =linelist
        self.caseId = int(caseid)

    def copy_data_cliff(self):

        linelist_df = pd.read_csv(self.lineList)
        cliff_columns = ["Age (Years)","Sex", "Travel History(Last 14 days)", "Where from", "Local/Imported",
                         "Flight No.",
                         "Date of arrival", "Symptoms(Yes/No)", "Date of onset of symptoms"]
        new_df = linelist_df[cliff_columns].copy()
        filterby = self.caseId
        filtered_df = new_df.iloc[filterby:,]

        '''
            check for null/empty values in age column and populate with the average
        '''
        print("Checking for null values....")
        filtered_df['Age (Years)'].fillna((math.floor(filtered_df['Age (Years)'].mean())), inplace=True)
        filtered_df['Where from'].fillna('N/A',inplace=True)
        print("Null values replaced with average of the age")
        # return filtered_df.head()
        # path_cliff = os.mkdir("Updated")
        print("Saving the rectified-copied data for cliff........")
        filtered_df.to_csv("cliff-today.csv", index=False)

