import sys
from copyCliff import CopyCliffData
import json

with open(sys.argv[1],'r') as f:
	data = json.load(f)
linelist = data["linelist"]

caseid = data["case_id"]
CopyCliffData(linelist,caseid).copy_data_cliff()
