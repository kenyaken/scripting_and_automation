from sub_county_recov import SubRecoveries
import sys
import json

with open(sys.argv[1],'r') as f:
    data = json.load(f)
recov = data["sub_recov"]
conf = data["confirmed_sub"]
daily_recoveries = data["recovered"]
todays_date = data["todays_date"]

SubRecoveries(recov,conf,daily_recoveries,todays_date).process_sub()
