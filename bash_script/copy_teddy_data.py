import pandas as pd
import math

class CopyTedData:

    def __init__(self, linelist, caseid):
        self.lineList = linelist
        self.caseId = int(caseid)

    def copy_data_ted(self):
        print("Reading data................")
        linelist_df = pd.read_csv(self.lineList)
        print("Successfully read data......")
        ted_columns = ["Age (Years)", "Sex", "County of Residence", "County where the case was Diagonised",
                       "Nationality",
                       "Sub county", "Ward", "Village/Estate", "Travel History(Last 14 days)", "Where from",
                       "Local/Imported",
                       "Flight No.",
                       "Date of arrival", "History of Contact with Covid-19 case", "Symptoms(Yes/No)",
                       "Date of onset of symptoms"]
        print("Creating an empty dataframe with columns to be copied...")
        new_df = linelist_df[ted_columns].copy()
        filterby = self.caseId
        filtered_df = new_df.iloc[filterby:, ]
        print(filtered_df.head())
        print(filtered_df["Age (Years)"].isna())
        # return filtered_df.head()
        # path_cliff = os.mkdir("Updated")

        '''
            check for null/empty values in age column and populate with the average
        '''
        filtered_df['Age (Years)'].fillna((math.floor(filtered_df['Age (Years)'].mean())), inplace=True)
        print(filtered_df["Age (Years)"].isna())

        try:
            filtered_df.to_csv("ted_copied.csv", index=False)
            print("Successfully copied your data!")
        except Exception as e:
            print("Error, could not save the data" + e.message)
