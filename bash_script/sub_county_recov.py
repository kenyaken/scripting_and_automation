import pandas as pd
from pathlib import Path
import os
class SubRecoveries:
    def __init__(self,sub_count_recov,sub_conf, daily_recov,todays_date):
        # self.sub_data = sub_count_list
        self.recoveries = int(daily_recov)
        self.todays_date = todays_date
        self.df = pd.read_csv(sub_count_recov)
        self.conf_df = pd.read_csv(sub_conf)
    
    def process_sub(self):
        conf_last_col = self.conf_df.iloc[:,-1]
        recov_last_col = self.df.iloc[:,-1]
        active_cases = conf_last_col- recov_last_col
        total_active = sum(active_cases)
        recov_ratio = self.recoveries/total_active
        print("rectifying negative values")
        for i in active_cases:
            if i<0:
                i=i*-1
        
        recov_sub_dist = round(active_cases*recov_ratio)
        self.df[self.todays_date] = recov_last_col + recov_sub_dist
        print("saving sub count data")
        os.makedirs("Updated_sub_recoveries")
        self.df.to_csv("Updated_sub_recoveries/time_series_ke_updated_subcounties_covid_19_recovery.csv",index=False)