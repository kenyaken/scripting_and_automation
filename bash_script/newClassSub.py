import pandas as pd
import numpy as np
from operator import add
from pathlib import Path
import os

# generate n random values summing up to x
def sum_to_x(n, x):
    values = [0.0, x] + list(np.random.uniform(low=0.0, high=x, size=n - 1))
    values = [round(i) for i in values]
    #     print(values)
    values.sort()
    return [values[i + 1] - values[i] for i in range(n)]


class ProcessSub:
    def __init__(self, linelist, county, sub_count, todays_date, case_id, cases_today):
        self.sub_df = pd.read_csv(sub_count)
        self.counties_df = pd.read_csv(county)
        self.line_df = pd.read_csv(linelist)
        self.todays_date = todays_date
        self.case_id = int(case_id)
        self.cases = int(cases_today)

    def list_to_lower(self, list_data):
        return ["".join(j.lower().replace("'", "").split()) for j in list_data]

    # adjusting the data from counties df
    def convert_df_lower(self, data, column_name):
        rectified_column = []
        for x in list(data[column_name]):
            rect = x.split()
            rect = "".join(rect)
            rectified_column.append(rect.lower())
        data['newCounty'] = rectified_column
        return data

    def process_data(self):

        end_row = self.case_id + self.cases
        # get sum of confirmed cases from linelist
        filtered_df = self.line_df.iloc[self.case_id:end_row, ]
        county_of_residence = list(filtered_df['County of Residence'])
        county_of_residence = self.list_to_lower(county_of_residence)
        county_sum = {x: county_of_residence.count(x) for x in county_of_residence}

        # get confirmed and not confirmed
        county_sum
        print("generating counties with confirmed")
        county_with_confirmed = []
        for key in county_sum:
            county_with_confirmed.append(key)

        print('------------------------------------------------------------')
        print("generating number of confirmed cases per day")
        confirmed_sum = []
        for value in county_sum.values():
            confirmed_sum.append(value)
        county_sum
        county_sum_df = pd.DataFrame(list(county_sum.items()))
        county_sum_df.columns = ['new_count', 'cases']
        county_sum_df

        # # Get the county index with confirmed cases and
        # # that without confirmed cases

        # In[5]:

        counties_df = self.convert_df_lower(self.counties_df, 'County')
        counties_with_conf = []
        for key in county_sum:
            county_with_confirmed.append(key)

        counties_df["CountyLower"] = self.list_to_lower(list(counties_df["County"]))
        counties_to_be_added = counties_df[counties_df['newCounty'].isin(county_with_confirmed)]['CountyLower']
        counties_not_conf = counties_df[~counties_df['newCounty'].isin(county_with_confirmed)]['CountyLower']
        # len(counties_to_be_added)
        index_to_be_added = list(counties_to_be_added.index)
        index_to_remain = list(counties_not_conf.index)
        # len(counties_to_be_added)
        counties_to_be_added
        not_conf = []
        for i in counties_not_conf:
            not_conf.append(i)
        not_added_df = pd.DataFrame(not_conf)
        not_added_df.columns = ['new_count']
        count_cases_df = county_sum_df.append(not_added_df, ignore_index=True)

        count_cases_df.fillna(value=0,
                              inplace=True)
        count_cases_df.sort_values(by=['new_count'], inplace=True)
        cases_list = []
        for i in count_cases_df['cases']:
            cases_list.append(i)
        cases_list

        sorted_df = self.sub_df.sort_values('County')
        sorted_df['County']
        groups = sorted_df.groupby('County')['Subcounty'].apply(list)
        groups
        df1 = groups.reset_index(name
                                 ='listvalues')
        sub_c_no = []
        print(len(df1.listvalues[0]))
        for i, v in enumerate(df1.County):
            sub_c_no.append(len(df1.listvalues[i]))
        # dataframe with subcounties in nairobi
        df1['subcounty_no'] = sub_c_no
        df1.subcounty_no[0]

        # df1.listvalues[i] for i in range(len(df1.listvalues))
        # sorted_df.sort_values(['County', 'Subcounty']).set_index(['County', 'Subcounty'])
        # sorted_df

        '''
            daily sum of cases per county,
            append to above df1 dataframe
            

        '''
        first_six = sorted_df.iloc[:6, -1]
        list(first_six.values)
        sub_no = []
        for i, v in enumerate(df1.subcounty_no):
            sub_no.append(v)
        sub_no
        prev_len = 0
        new_sub = []  # holds the updated data
        '''
        the code below extracts the subcounties and performs a tweak to the data
        i.e should update the cases and return the distributed cases per sub 
        county

        '''
        for i in df1.subcounty_no:
            new_len = prev_len + i
            sub_c_cases = sorted_df.iloc[prev_len:new_len, -1]
            new_sub.append(list(sub_c_cases.values))
            prev_len += i
        df1_county_lower = self.list_to_lower(df1['County'])
        df1['lowerCounty'] = df1_county_lower
        df1['daily_cases'] = cases_list

        # # Ordering of the sorted data frame does not matter
        # ### perform the computations minding not the sorted df
        # code to generate random values for n number of subcounties based on x sum of cases
        # in that county
        updated_data = []
        for i in range(len(new_sub)):
            update = new_sub[i]  # list to be updated
            n = len(new_sub[i])
            x = df1['daily_cases'][i]
            rand_sub_data = sum_to_x(n, x)
            res_list = [sum(i) for i in zip(update, rand_sub_data)]
            updated_data.append(res_list)
        merged_list = []
        for l in updated_data:
            merged_list += l
        new_sub
        folder = Path.cwd() / 'sub_conf_folder'
        os.mkdir(folder)
        # self.todays_date
        sorted_df[self.todays_date] = merged_list
        sorted_df.to_csv(folder / "time_series_ke_updated_subcounties_covid_19_confirmed.csv", index=False)
