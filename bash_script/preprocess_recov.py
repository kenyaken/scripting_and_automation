import pandas as pd
import os

# # initialize your variables and data
# todays_date = '5/19/21'
# actual_recoveries = 356
# recoveries = 'time_series_ke_covid_19_recovery_cumulative_sum.csv'
# confirmed = 'time_series_ke_covid_19_confirmed_cumulative_sum.csv'
# df_recov = pd.read_csv(recoveries)
# confirmed_df = pd.read_csv(confirmed)


class ProcessRecovered:
    def __init__(self, recoveries, recoveries_df, confirmed, todays_date):
        self.df_recov = pd.read_csv(recoveries_df)
        self.confirmed_df = pd.read_csv(confirmed)
        self.todays_date = todays_date
        self.actual_recoveries = int(recoveries)

    def process_data(self):

        # extract the last columns
        conf_last_col = self.confirmed_df.iloc[:, -1]
        recov_last_col = self.df_recov.iloc[:, -1]

        # compute for the active cases

        active_cases = conf_last_col - recov_last_col

        total_active_cases = sum(active_cases)
        # print(active_cases)
        recovered_ratio = self.actual_recoveries / total_active_cases

        # compute the distribution of recoveries per county
        recoveries_distribution = round(active_cases * recovered_ratio)
        computed_recov = sum(recoveries_distribution)
        self.df_recov[self.todays_date] = recov_last_col + recoveries_distribution
        print("Saving your data........")
        os.mkdir('updated_recovered')
        self.df_recov.to_csv("updated_recovered/time_series_ke_covid_19_recovery_cumulative_sum.csv",index=False)

        if self.actual_recoveries < computed_recov:
            print("Data to be adjusted by Subtracting {} to the data".format(computed_recov - self.actual_recoveries))
        elif self.actual_recoveries > computed_recov:
            print("Data to be adjusted by adding {} from the data".format(self.actual_recoveries - computed_recov))
        else:
            print("Data saved successfully!")
