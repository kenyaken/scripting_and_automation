from preprocess_recov import ProcessRecovered
import sys
import json

with open(sys.argv[1],'r') as f:
	data = json.load(f)
recoveries = data["recovered"]
recoveries_df = data["recovered_counties"]
confirmed = data["confirmed_counties"]
todays_date = data["todays_date"]
ProcessRecovered(recoveries,recoveries_df,confirmed,todays_date).process_data()
