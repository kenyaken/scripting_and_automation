# scripting_and_automation

# manipulating the json file
### NB DO NOT tamper with the keys i.e linelist, todays_date .. etc if you are to do so change the keys in ALL files to reflect the change
open data.json file
manipulate the values in json fields
for line list and other keys for filepaths, ensure the filepaths are correct, to minimize it, store all the csv files in the folder ..
referencing the filepath as follows : 'data/filename' 
i.e : "linelist": "data/csv_file_with_data.csv"

for other parameters/inputs as confirmed cases just edit and put the daily cases (confirmed,recoveries) or dates (todays_date)

# running the bash file
`bash bashname.sh`

i.e 
`bash script.sh`

