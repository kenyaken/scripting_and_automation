'''
    use this to run or process data
'''
import sys
from preprocess_county_data import ProcessConfirmedCounties
import json


'''
run this code on the command line to generate your data
ensure that your data is in the folder containing this scripts
to run this on the command line type : python3 main.py and then type the filenames( linelist, confirmed cases) in that order
then finally type current date in this format YYYY-MM-DD 
'''
with open(sys.argv[1], 'r' ) as f:
	data = json.load(f)
line_list = data["linelist"]
# county_cum_confirmed = sys.argv[2]
confirmed_cases = data["confirmed_counties"]
todays_date = data["todays_date"]
filter_by = data["case_id"]
to = data["confirmed"]
ProcessConfirmedCounties(line_list,confirmed_cases,todays_date,filter_by,to).process_data()

