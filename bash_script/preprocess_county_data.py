import pandas as pd
import os.path
from pathlib import Path
from os import path
import random
from datetime import datetime
# todays_date = '2021-05-17'
#
# line_list = "COVID 19 LINELIST AS AT 18TH MAY 21.csv"
# county_cum_confirmed = "time_series_ke_covid_19_confirmed_cumulative_sum.csv"
# confirmed_cases = "time_series_ke_covid_19_confirmed_cumulative_sum.csv"
# print("----------------------------------------------------------")
# print("Reading csv data")
# df = pd.read_csv(line_list)
# df_confirmed = pd.read_csv(county_cum_confirmed)
# print('Finished reading data')
# last_column_name = list(df_confirmed)[-1]
class ProcessConfirmedCounties:
    def __init__(self, linelist, confirmed, date,filter_by,cases):
        print("Checking if the files exists.....")
        if os.path.isfile(confirmed):
            self.df_confirmed = pd.read_csv(confirmed)
            print("Confirmed file read successfully")
        else:
            print("Confirmed file does not exist, Ensure the file is in the same directory as the script!")
        if os.path.isfile(confirmed):
            self.df = pd.read_csv(linelist)
            print("Confirmed file read successfully")
        else:
            print("Linelist file does not exist, Ensure the file is in the same directory as the script!")
        # self.df = pd.read_csv(linelist)
        self.filter_by = int(filter_by)-1
        self.cases = int(cases)
        self.todays_date = date
        self.last_column_name = list(self.df_confirmed)[-1]

    def convert_df_lower(self,data, column_name):
        rectified_column = []
        for x in list(data[column_name]):
            rect = x.split()
            rect = "".join(rect)
            rectified_column.append(rect.lower())
        data['newCounty'] = rectified_column
        return data

    def list_to_lower(self,list_data):
        return ["".join(j.lower().replace("'", "").split()) for j in list_data]
    def process_data(self):
        print('----------------------------------')

        print("Converting county column to lower case")
        df_confirmed = self.convert_df_lower(self.df_confirmed, 'County')
        to = self.filter_by+self.cases
        filtered_df = self.df.iloc[self.filter_by:to,]
        #print(filtered_df)
        county_of_residence = list(filtered_df['County of Residence'])
        #county_of_residence = list(self.df[self.df['Date of lab confirmation'] == self.todays_date]['County of 		#Residence'])
        county_of_residence = self.list_to_lower(county_of_residence)
        df_confirmed["CountyLower"] = self.list_to_lower(list(df_confirmed["County"]))
        county_sum = {x: county_of_residence.count(x) for x in county_of_residence}
        last_column_county = df_confirmed.iloc[:, -3]

        print('------------------------------------------------------------')
        print("generating counties with confirmed")
        county_with_confirmed = []
        for key in county_sum:
            county_with_confirmed.append(key)


        print('------------------------------------------------------------')
        print("generating number of confirmed cases per day")
        confirmed_sum = []
        for value in county_sum.values():
            confirmed_sum.append(value)

        print('----------------------------------------------------')
        print('Generating a list of counties to be added and those with no records for the day')
        counties_to_be_added = df_confirmed[df_confirmed['newCounty'].isin(county_with_confirmed)]['CountyLower']
        counties_not_conf = df_confirmed[~df_confirmed['newCounty'].isin(county_with_confirmed)]['CountyLower']


        print('------------------------------------------------------------')
        print("generating indices for counties with confirmed")
        index_to_be_added = list(counties_to_be_added.index)
        index_to_remain = list(counties_not_conf.index)

        counties_prev_changed = {}
        for i in index_to_be_added:
            counties_prev_changed["".join(df_confirmed["County"][i].lower().replace("'", "").split())] = last_column_county[i]
        current_cumulative_changed = {}
        # print(last_column_name)
        # print(list(county_sum.values())[0])
        for k, v in counties_prev_changed.items():
            # print(v)
            current_cumulative_changed[k] = v + county_sum[k]
        # print(current_cumulative_changed)

        counties_prev_unchanged = {}
        for i in index_to_remain:
            counties_prev_unchanged["".join(df_confirmed["County"][i].lower().replace("'", "").split())] = last_column_county[i]

        print('------------------------------------------------------------')
        print("generating a sorted dataframe with new cumulative sum ")
        combined_dict = {**current_cumulative_changed, **counties_prev_unchanged}
        counties = combined_dict.keys()
        vum_sum = combined_dict.values()
        new_df = pd.DataFrame(list(zip(counties, vum_sum)), columns=['County', 'Date'])
        new_df = new_df.sort_values(by="County", ascending=True)
        new_df.reset_index(drop=True, inplace=True)


        print('------------------------------------------------------------')
        print("Preprocessing finished!")

        print('------------------------------------------------------------')
        print("Writing data to the local storage.......")
        df_confirmed[self.todays_date] = new_df["Date"]
        today_time = datetime.now().strftime("%d %b %Y ")

        del df_confirmed['newCounty']
        del df_confirmed['CountyLower']
        rand_no = random.randint(0,9)
        today_time = today_time+str(rand_no)
        # print(str(today_time))
        if os.path.isdir('new_folder'):
            # os.mkdir(new_folder/today_time)
            # df_confirmed.to_csv("new_folder/today_time/time_series_ke_covid_19_confirmed_cumulative_sum.csv")
            outpath = Path.cwd() / 'new_folder' / today_time
            os.mkdir(outpath)
            df_confirmed.to_csv(outpath/'time_series_ke_covid_19_confirmed_cumulative_sum.csv',index=False)
        else:
            os.mkdir('county_conf_update')
            df_confirmed.to_csv("county_conf_update/time_series_ke_covid_19_confirmed_cumulative_sum.csv",index=False)

        print("Data saved successfully!Check on the folder written {}".format(today_time))

#
# os.mkdir("updated_data")
# # print(df_confirmed['newCounty'])


