from newClassSub import ProcessSub
import sys
import json

with open(sys.argv[1],'r') as f:
	data = json.load(f)
linelist = data["linelist"]

caseid = data["case_id"]
county = data["confirmed_counties"]
subcounty = data["confirmed_sub"]
todays_date = data["todays_date"]
case_id = data["case_id"]
cases_today = data["confirmed"]

ProcessSub(linelist,county,subcounty,todays_date,case_id,cases_today).process_data()
