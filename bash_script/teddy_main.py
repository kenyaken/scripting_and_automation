import sys
from copy_teddy_data import CopyTedData
import json

with open(sys.argv[1],'r') as f:
	data = json.load(f)
linelist = data["linelist"]

caseid = data["case_id"]

CopyTedData(linelist,caseid).copy_data_ted()
